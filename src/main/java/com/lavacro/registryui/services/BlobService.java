package com.lavacro.registryui.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.InputStream;

import java.net.URL;
import java.net.URLConnection;

@Service
public class BlobService extends RegistryService {
	private static final Logger logger = LoggerFactory.getLogger(BlobService.class);

	public InputStream download(final String image, final String digest) {
		try {
			URLConnection connection =new URL(String.format("%s://%s/v2/%s/blobs/%s", PROTOCOL, HOST, image, digest)).openConnection();
			connection.addRequestProperty("Accept", ACCEPT_HEADER);
			connection.setDoOutput(true);

			return connection.getInputStream();
		} catch(IOException e) {
			logger.error(e.getMessage());
			return null;
		}
	}
}
