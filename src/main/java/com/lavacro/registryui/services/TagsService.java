package com.lavacro.registryui.services;

import com.lavacro.registryui.model.GenericResponse;
import com.lavacro.registryui.model.Tags;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Component
@Slf4j
public class TagsService extends RegistryService {

	/**
	 * Retrieves all the tags for a given image
	 * Images are normally versioned. If not versioned, images will default to a "latest" version. If a subsequent
	 * build is uploaded as the new "latest", container orchestrators such as Kubernetes will not realize that it
	 * is different than previously, and a pull policy of "always" would be required to effectuate the change. The
	 * registry API responds with a JSON object that contains a "tags" attribute, which is a list of the tags:
	 * {"name":"finances","tags":["1.8.1"]}
	 * 	The format of the request is - http://myregistry:port/v2/{reponame}/tags/list and pagination has the standard
	 * 	?n={number} appended to the URL.
	 *
	 * @param image A string representing the image (repo) name
	 * @return A Tags object with a list of strings as a member field
	 */
	public Tags tags(final String image) {
		Tags tags = new Tags();
		tags.setName(image);
		String endpoint = String.format("%s://%s/v2/%s/tags/list", PROTOCOL, HOST, image);
		boolean needMore = true; // I'm not paginating, but there's no guarantee that all results will be returned
		List<String> tagsList = new ArrayList<>();

		try {
			while(needMore) {
				ResponseEntity<Tags> result = new RestTemplate()
						.exchange(endpoint, HttpMethod.GET, null, Tags.class);
				tags = result.getBody();
				assert tags != null;
				tagsList.addAll(tags.getTags());

				String link = getLink(result);
				if (link == null) {
					needMore = false;
				} else {
					endpoint = String.format("%s://%s%s", PROTOCOL, HOST, link);
				}
			}

			// now cache results; unfortunately, Redis doesn't have an "addAll()" equivalent, so iterate
			tags.setCode(0);
			tags.setTags(tagsList.stream().sorted().collect(Collectors.toList())); // results may not be sorted, so sort
		} catch(Exception e) {
			tags = new Tags();
			tags.setCode(1);
			tags.setMessage(e.getMessage());
		}
		return tags;
	}

	/**
	 * Deletes a tag from a repository
	 *
	 * @param image Name of the image/repository
	 * @param tag Version of the tag to delete
	 * @return An object with a success/fail response
	 */
	public GenericResponse deleteTag(final String image, final String tag) {
		GenericResponse response = new GenericResponse();
		String sha = manifest(image, tag);
		assert sha != null;

		String endpoint = String.format("%s://%s/v2/%s/manifests/%s", PROTOCOL, HOST, image, sha);
		HttpHeaders reqHeaders = new HttpHeaders();
		reqHeaders.add(HttpHeaders.ACCEPT, ACCEPT_HEADER);
		reqHeaders.add(HttpHeaders.ACCEPT, OCI_HEADER);
		HttpEntity<HttpHeaders> httpEntity = new HttpEntity<>(reqHeaders);
		try {
			ResponseEntity<String> result = new RestTemplate()
					.exchange(endpoint, HttpMethod.DELETE, httpEntity, String.class);
			if(result.getStatusCode() == HttpStatus.ACCEPTED) {
				response.setCode(0);
				response.setMessage("ok");
			} else {
				response.setCode(1);
				response.setMessage(String.format("Response code: %s", result.getStatusCode()));
			}
		} catch(Exception e) {
			response.setCode(1);
			response.setMessage(e.getMessage());
			log.error(e.getMessage());
		}
		return response;
	}
}
