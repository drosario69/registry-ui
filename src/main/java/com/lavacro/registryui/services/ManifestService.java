package com.lavacro.registryui.services;

import com.lavacro.registryui.model.Manifest;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class ManifestService extends RegistryService {

	public Manifest getLayers(final String image, final String tag) {
		Manifest manifest;
		String endpoint = String.format("%s://%s/v2/%s/manifests/%s", PROTOCOL, HOST, image, tag);
		HttpHeaders reqHeaders = new HttpHeaders();
		reqHeaders.set("Accept", ACCEPT_HEADER);
		HttpEntity<HttpHeaders> httpEntity = new HttpEntity<>(reqHeaders);
		try {
			ResponseEntity<Manifest> result = new RestTemplate()
					.exchange(endpoint, HttpMethod.GET, httpEntity, Manifest.class);
			manifest = result.getBody();
			if(manifest == null) {
				manifest = new Manifest();
				manifest.setCode(1);
				manifest.setMessage("No results");
			}
			manifest.setCode(0);
		} catch(Exception e) {
			manifest = new Manifest();
			manifest.setCode(1);
			manifest.setMessage(e.getMessage());
		}
		return manifest;
	}
}
